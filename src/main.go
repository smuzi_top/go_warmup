package main

import(
	models "domain/models"
	httpService "domain/services"
	databaseService "domain/services/database"
	"fmt"
	"html/template"
	"io/ioutil"
    "net/http"
	"log"
	"encoding/json"
	"runtime"
)

var appConfig *models.AppConfig;

func main() {

	http.HandleFunc("/", handleHomePageHandler)
	http.HandleFunc("/content/", handleContentHandler)
	http.HandleFunc("/api/checkit", handleApiCheckRequest)
	
	log.Fatal(http.ListenAndServe(":8080", nil));
}

func init() {
	appConfig = readAppConfig()
}

func loadContent(filePath string) ([]byte, error) {
    body, err := ioutil.ReadFile(filePath)
    if err != nil {
        return nil, err
    }
    return body, nil
}

func handleHomePageHandler(w http.ResponseWriter, r *http.Request) {
	p, _ := loadContent("wwwroot/index.html")
	t, _ := template.ParseFiles("wwwroot/index.html")


	// forward to UI a cooldown timer to avoid click-spam
	var attemptCooldown int = *databaseService.GetTrialCheckCooldown();

	if (attemptCooldown < 30) {
		attemptCooldown = (attemptCooldown - 30) * -1;
	} else {
		attemptCooldown = 0
	}

	pageTemplate := &models.PageTemplateModel{Title: "Golang startup", Body: p, Cooldown: attemptCooldown, Runtime: runtime.Version()  };

	w.Header().Set("Content-Type", "text/html; charset=utf-8")

	err := t.Execute(w, pageTemplate)
	
	if (err != nil) {
		log.Fatal(err);
	}
}

func handleContentHandler(w http.ResponseWriter, r *http.Request) {

	var contentTargetResource string = r.URL.Path

	var fullPathToResource = fmt.Sprintf("wwwroot/%s", contentTargetResource) 

	p, _ := loadContent(fullPathToResource);

	w.Header().Set("Content-Type", "text/css")

	w.Write(p);

}


func handleApiCheckRequest(w http.ResponseWriter, r *http.Request) {

	if appConfig == nil {
		fmt.Println("Settings from appconfig.json were not loaded, aborting");
		return
	}
	
	w.Header().Set("Content-Type", "application/json")

	if (databaseService.IsCooldownPresent()) {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	
	databaseService.SetTrialCheckCooldown()

	checkResult := httpService.CheckTarget(appConfig.CheckingNodes)

	jsonResult, err := json.Marshal(&checkResult)	

	_, errWriteToResponse := w.Write(jsonResult)

	if (errWriteToResponse != nil) {
		log.Fatal(err);
		return
	}
}

func readAppConfig() *models.AppConfig {

	appConfig, err := ioutil.ReadFile("appconfig.json");

	if (err != nil) {
		fmt.Printf("Could not read appconfig.json: %s", err)
		return &models.AppConfig{}
	}

	var appConfigObject models.AppConfig
	
	marshallingError := json.Unmarshal([]byte(appConfig), &appConfigObject)

	if (marshallingError != nil) {
		fmt.Printf("Could not parse appconfig.json: %s", marshallingError)
		return &models.AppConfig{}
	}

	return &appConfigObject;
	
}
