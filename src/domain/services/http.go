package http

import(
	models "domain/models"
	providers "providers"
	"log"
	"bytes"
	"encoding/json"
)

func CheckTarget(checkNodes []models.CheckingNodeModel) []models.CheckTargetResponseModel {

	userAgent := "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)"

	jsonRequestData, err := json.Marshal(models.CheckTargetRequestModel {
		UrlId: 1,
		Port: 443,
		Host: "www.tn.ru",
		Url: "/",
		UseSsl: true,
		UserAgent: &userAgent,
		AdvancedHeaders: nil,
		HttpRequestMethod: 1,
	})

	if err != nil {
		log.Fatal(err)
		return nil
	}

	var checkResult *models.CheckTargetResponseModel;

	checkResults := make([]models.CheckTargetResponseModel, 0)

	for _, node := range checkNodes {

		apiResponseBytes, err := providers.InvokeHttpRequest("POST", "http://" + node.Ip + "/api/check/single", bytes.NewBuffer(jsonRequestData));

		if err != nil {
			continue
		}

		checkResult = &models.CheckTargetResponseModel{}

		marshallingError := json.Unmarshal(apiResponseBytes, &checkResult)

		if marshallingError != nil {
			continue
		}		

		checkResult.Node = &models.CheckingNodeModel{Name: node.Name}

		checkResults = append(checkResults, *checkResult)

	}

	return checkResults;
}