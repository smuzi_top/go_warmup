package database

import(
	databaseProvider "providers/database"
	"time"
)

func IsCooldownPresent() bool {
	
	var cooldown *int = GetTrialCheckCooldown();

	if (cooldown == nil) {
		return false
	} 

	if (*cooldown <= 30) {
		return true
	} else {
		return false
	}

}

func GetTrialCheckCooldown() *int {

	var lastTrialCheckDate *time.Time = databaseProvider.GetLastAttempDate();

	if (lastTrialCheckDate == nil) {
		return nil;
	}
	
	timeLayout := *databaseProvider.GetTimeLayout();

	timeNowString := time.Now().Format(timeLayout)

	timeNowDate, _ := time.Parse(timeLayout, timeNowString)

	elapsed := timeNowDate.Sub(*lastTrialCheckDate)

	elapsedSeconds := int(elapsed/time.Second)

	return &elapsedSeconds;

}

func SetTrialCheckCooldown() {

	timeLayout := *databaseProvider.GetTimeLayout();

	timeNowString := time.Now().Format(timeLayout)

	timeNowDate, _ := time.Parse(timeLayout, timeNowString)

	databaseProvider.SetLastAttempDate(&timeNowDate);

}