package models

type PageTemplateModel struct {
	Title string
	Cooldown int
	Body []byte
	Runtime string
}