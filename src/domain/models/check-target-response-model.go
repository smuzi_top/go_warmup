package models

type CheckTargetResponseModel struct {

	UrlId int						`json:"urlId"`

	DelayDns *int					`json:"delayDns"`

	DelayInitialConnect *int		`json:"delayInitialConnect"`

	DelaySsl *int 					`json:"delaySsl"`

	DelayFirstByte *int				`json:"delayFirstByte"`

	DelayContentDownload *int		`json:"delayContentDownload"`

	DownloadedContentLength *int	`json:"downloadedContentLength"`

	ResponseCode *int				`json:"responseCode"`

	ResponseHeaders *string			`json:"responseHeaders"`

	Node *CheckingNodeModel			`json:"node"`
}