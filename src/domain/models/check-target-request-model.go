package models

type CheckTargetRequestModel struct {

	UrlId int

	Port int

	Host string

	Url string

	UseSsl bool

	UserAgent *string

	AdvancedHeaders *string

	HttpRequestMethod int
}
