﻿//$(class, id, element tag name) selector
window.__proto__.$ = function (selector) {

	if (!selector)
		return Array(0);

	if (selector == 'window')
		return window;
	else if (selector == 'document')
		return document;

	var firstChar = selector[0];

	var elems = {};

	switch (firstChar) {
		case '#':
			elems = document.getElementById(selector.substr(1));
			break;

		case '.':
			elems = document.getElementsByClassName(selector.substr(1));
			break;

		case '[':
			elems = document.querySelector(selector);
			break;

		default:
			elems = document.getElementsByTagName(selector);
			break;
	}

	if (elems) {

		var elemsType = Object.prototype.toString.call(elems);

		if (elemsType === '[object HTMLCollection]') {

			if (elems.length == 1) {
				return elems[0];
			}
			else if (elems.length > 1) {
				return elems;
			}
		}
		else {
			return elems;
		}
	}

};

//overlay html elements array
window.__proto__._blockUiFrames = [];

//block element
//if htmlElement was not set - document.body will be used as default
window.__proto__.BlockUI = function (htmlElement, preloadLabel) {

    var isBodyElement = false;

    if (typeof htmlElement === 'object') {
        
    }
    else if (typeof htmlElement === 'string') {

        isBodyElement = true;

        preloadLabel = htmlElement;

        htmlElement = document.body;
    }
    else if (typeof htmlElement === 'undefined') {

        isBodyElement = true;

        htmlElement = document.body;
    }

	var blockingFrameId = Math.abs(Math.floor(Math.random() * (10000 - 99999)) + 10000);

    htmlElement.setAttribute('blockingid', blockingFrameId);

	var elemOffset = {
		left: htmlElement.offsetLeft,
		top: htmlElement.offsetTop
	};

	var elemSize = {
		width: isBodyElement ? '100%' : htmlElement.offsetWidth + 'px',
		height: isBodyElement ? '100%' : htmlElement.offsetHeight + 'px'
	};

	var blockingFrameElement = document.createElement('div')

    blockingFrameElement.innerHTML = preloadLabel ?
        '<div class="preload_container"><div class="_preloader"></div><div class="_preloader_label">' + preloadLabel + '</div></div>' :
        '<div class="preload_container"><div class="_preloader"></div></div>';


	blockingFrameElement.classList.add('uiblockingframe');

	blockingFrameElement.style.top = elemOffset.top + 'px';
	blockingFrameElement.style.left = elemOffset.left + 'px';

	blockingFrameElement.style.width = elemSize.width;
	blockingFrameElement.style.height = elemSize.height;

	if (!isBodyElement)
		blockingFrameElement.style.position = "absolute";

	_blockUiFrames.push({ blockingid: blockingFrameId, elem: blockingFrameElement });

	document.body.insertBefore(blockingFrameElement, document.body.firstChild);

	window.setTimeout(function () {
		blockingFrameElement.classList.add('popover');
	}, 10);

}

//unblock previosly blocked element
//if htmlElement not set - document.body as default
window.__proto__.UnblockUI = function (htmlElement) {

	if (!_blockUiFrames ||
		!_blockUiFrames.length) {
		console.error('Could not remove UI blocking frame: descriptor not found');
		return;
	}

	if (!htmlElement)
		htmlElement = document.body;

    var blockingId = htmlElement.getAttribute('blockingid');

	var _blockingFrame,
		_blockingFrameIndex;

	for (var i = 0; i < _blockUiFrames.length; i++) {

		_blockingFrame = _blockUiFrames[i].elem;

		if (_blockUiFrames[i].blockingid == blockingId) {
			_blockingFrameIndex = i;
			_blockingFrame.classList.remove('popover');
			_blockingFrame.classList.add('destroy');			
			break;
		}
	}

	if (_blockingFrame) {

		_blockUiFrames.splice(_blockingFrameIndex, 1);

		window.setTimeout(function () {
			_blockingFrame.parentNode.removeChild(_blockingFrame);
		}, 555);

	}
	else {
		console.error('Could not set animation to UI blocking frame: frame not found');
		return;
	}

}

//trigger an event assigned to element
window.__proto__.DispatchEvent = function (element, eventName) {

	if (document.createEvent) {
		var evt = document.createEvent('HTMLEvents');
		evt.initEvent(eventName, false, true);
		element.dispatchEvent(evt);
	}
	else
		element.fireEvent(eventName);
};

//set element disabled or remove disable attribute (toggle 'disabled' attr)
window.__proto__.DisableElement = function (element) {

	const DISABLED_ATTRIBUTE = 'disabled';

	if (!element)
		return;

	var attributes = element.attributes;

	var isElementAlreadyDisabled = false;

	for (var i = (attributes.length - 1); i >= 0; i--) {
		if (attributes[i].name == DISABLED_ATTRIBUTE) {
			isElementAlreadyDisabled = true;
			break;
		}		
	}

	if (isElementAlreadyDisabled) {
		element.removeAttribute(DISABLED_ATTRIBUTE);
	}
	else {
		element.setAttribute(DISABLED_ATTRIBUTE, DISABLED_ATTRIBUTE);
	}	
}

//check if element is disabled
window.__proto__.IsElementDisabled = function (element) {

	const DISABLED_ATTRIBUTE = 'disabled';

	if (!element)
		return false;

	var attributes = element.attributes;

	for (var i = (attributes.length - 1); i >= 0; i--) {
		if (attributes[i].name == DISABLED_ATTRIBUTE)
			return true;		
	}

	return false;
}