package http

import(
	"fmt"
	"io/ioutil"
    "net/http"
	"bytes"
)

func InvokeHttpRequest(method string, apiUrl string, data *bytes.Buffer) ([]byte, error) {

	client := &http.Client{}

	req, _ := http.NewRequest(method, apiUrl, data)

	req.Header.Set("Content-Type", "application/json")

	resp, err := client.Do(req)
	
	if err != nil {
		fmt.Printf("Can not invoke an endpoint (IP %s). Error: %s", apiUrl, err)
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		fmt.Printf("Can not read ednpoint's (IP %s) response body. Error: %s", apiUrl, err)
		return nil, err
	}		

	return body, nil
}