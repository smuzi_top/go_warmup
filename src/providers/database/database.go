package database

import(
	"fmt"
	"time"
	"io/ioutil"
)

func GetTimeLayout() *string {
	var layout string = "2006-01-02T15:04:05.000Z";
	return &layout
}

func SetLastAttempDate(date *time.Time) {	

	stringDate := date.Format(*GetTimeLayout())

	stringBytes := []byte(stringDate)

	err := ioutil.WriteFile("database", stringBytes, 0600)

	if err != nil {		
		fmt.Printf("Can not write last attempt date to database file. Error: %s", err)
		return
	}
	
}

func GetLastAttempDate() *time.Time {
	
    body, err := ioutil.ReadFile("database")

	if err != nil {		
		fmt.Printf("Can not read data from database file. Error: %s", err)
		return nil
	}

	time, _ := time.Parse(*GetTimeLayout(), string(body))

	return &time;

}